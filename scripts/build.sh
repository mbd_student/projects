#!/bin/bash

echo "BUILD public"
cd ..
rm -rf _output
quarto render 
cp -r projects/* _output
echo "FINISHED"
